#include "bot.h"
#include <math.h>
#include <stdlib.h>

double mex;
double mey;

int size( const void *a, const void *b)
{
    double massa = ((struct player *)a) ->totalMass;
    double massb = ((struct player *)b) ->totalMass;
    return massa - massb;   
}

int dist( const void *a, const void *b)
{
    double ax = ((struct food *)a) ->x;
    double ay = ((struct food *)a) ->y;
    double bx = ((struct food *)b) ->x;
    double by = ((struct food *)b) ->y;
    double dista = sqrt((mex - ax)*(mex - ax) + (mey - ay)*(mey - ay));
    double distb = sqrt((mex - bx)*(mex - bx) + (mey - by)*(mey - by));
    return dista - distb;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    mex = me.x;
    mey = me.y;
    
    qsort(foods, nfood, sizeof(struct food), dist);
    
    act.dx = (foods[0].x - me.x);
    act.dy = (foods[0].y - me.y);
    
    switch (nfood)
    {
        case 0 :
            act.dx = (2500 - me.x); //couldn't test if it works
            act.dy = (2500 - me.y);
        default :
            switch (nplayers)
            {
                case 0 :
                    act.dx = (foods[0].x - me.x);
                    act.dy = (foods[0].y - me.y);
                    act.fire = 1;
                    act.split = 1;
                break;
                default :
                    qsort(players, nplayers, sizeof(struct player), size);
                    switch(me.ncells)
                    {
                        case 1:
                            if(players[0].totalMass < me.totalMass)
                            {
                                act.dx = (players[0].x - me.x);
                                act.dy = (players[0].y - me.y);
                            }
                            if(players[0].totalMass > me.totalMass)
                            {
                                act.dx = (players[0].x + me.x);
                                act.dy = (players[0].y + me.y);
                            }
                        break;
                        default :
                            if(players[0].ncells > me.ncells)
                            {
                                if(players[0].totalMass < me.totalMass)
                                {
                                    act.split = 0;
                                    act.dx = (players[0].x - me.x);
                                    act.dy = (players[0].y - me.y);
                                }
                                else
                                {
                                    act.dx = (players[0].x + me.x);
                                    act.dy = (players[0].y + me.y);
                                }
                            }
                            else
                            {
                                act.dx = (players[0].x + me.x);
                                act.dy = (players[0].y + me.y);
                            }
                    }
            }        
    }
    
    act.fire = 0;
    act.split = 0;
    
    return act;
}

